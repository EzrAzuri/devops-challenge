## Highly Scalable Infrastructure Suggestions for BlogApp

### Requirements:
1. Able to handle 1,000,000 of concurrent users.

### Solutions:
This suggestions will heavily use AWS services for easier integration at the cost of further locked into AWS.

![Screenshot](T4.png)

#### 1. Elastic Load Balancers
Distribute incoming traffics across multiple compute unit/containers. This will improves the performance and availability. 

#### 2. Auto Scaling Groups
Scale the number of compute unit based on demand and the optimal resource utilization automatically and set schedules based on predicted time of high concurrent users.

#### 3. Amazon Aurora Global Database
A global database with the ability to be replicate across multiple AWS regions and enabling low-latency global reads. 

#### 4. Amazon ECS or EKS
Container orchestration and auto-scaling to manage containers and ensure high availability.

#### 5. Amazon Route 53
A scalable domain name system (DNS) web service that provides global traffic management, routing requests to the nearest AWS region and distributing traffic across multiple endpoints.

#### 6. Amazon CloudFront: 
A content delivery network (CDN) service that caches and delivers content, reducing latency and improving the performance of the application for users across the globe.