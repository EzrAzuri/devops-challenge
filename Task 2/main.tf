provider "aws" {
  region = "ap-southeast-1"
  profile ="default"
}

resource "aws_security_group" "BlogApp_sg" {
  name        = "BlogApp_sg"
  description = "Allow inbound traffic for the Node.js web app"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 3000
    to_port     = 3000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "BlogApp_server"{
      ami = "ami-0df7a207adb9748c7"        
      instance_type = "t2.micro"
      tags = {
            Name = "BlogApp_server"
      }
      vpc_security_group_ids = [aws_security_group.BlogApp_sg.id]
      user_data = <<-EOF
                  #!/bin/bash
                  sudo apt update
                  curl -fsSL https://deb.nodesource.com/setup_20.x | sudo -E bash - &&\
                  sudo apt-get install -y git nodejs
                  git clone https://bitbucket.org/fusang-external/devops-challenge.git
                  cd devops-challenge/BlogApp
                  npm install
                  npm run start
                  EOF
}
