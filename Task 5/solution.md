## Recover BlogApp from Disaster

### Requirements
1. Assume application hosted in Tokyo and need to be recovered in Singapore.

### Solutions
Most of the solutions provided only usable when practiced before disaster happened.

#### 1. AMI and Snapshot Backup
Regularly create Amazon Machine Image (AMI) of the related EC2 instances, snapshots of EBS attached to the EC2 and the snapshot of the database. Copy these images and snapshots to the Singapore region and restore the data when needed.

#### 2. Cross-Region Replication
Set up cross-region replication for database and file storage. For example, Amazon Rds have the ability for Multi-AZ deployment.

#### 3. Global Traffic Management 
Use Amazon Route 53 to automatically route traffics to Singapore when Tokyo region become unavailable.

#### 4. Continous Data Backup
Use AWS Data Pipeline or AWS Database Migration Service to automate data backup and synchronization.

#### 5. Infrastructure as Code
Define the infrastructure using Terraform and store it in Source Code Management tool such as GitLab. Recreate the infrastructure to Singapore using the same template used in Tokyo.