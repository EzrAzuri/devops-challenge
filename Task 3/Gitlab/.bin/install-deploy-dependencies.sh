echo "Checking git"
if ! command -v git &> /dev/null; then
    echo "Git is not installed. Installing Git..."
    sudo apt-get update
    sudo apt-get install git -y
    echo "Git has been installed successfully."
else
    echo "Git is already installed."
fi

echo "Checking nodejs"
if ! command -v node &> /dev/null; then
    echo "Node.js is not installed. Installing Node.js..."
    curl -fsSL https://deb.nodesource.com/setup_20.x | sudo -E bash -
    sudo apt-get install -y nodejs
    echo "Node.js has been installed successfully."
else
    echo "Node.js is already installed."
fi

REPO_URL="https://gitlab.com/EzrAzuri/devops-challenge.git"
PROJECT_DIR="devops-challenge/BlogApp"
START_COMMAND="npm run start"
git clone $REPO_URL $PROJECT_DIR
cd $PROJECT_DIR
npm install
$START_COMMAND
